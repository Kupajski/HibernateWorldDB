import models.City;
import repository.cityRepository;
import repository.countryRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();


        cityRepository cityRepository = new cityRepository(entityManager);
        countryRepository countryRepository = new countryRepository(entityManager);
        State state = State.INIT;
        Scanner scanner = new Scanner(System.in);

   //     cityRepository.findAvgPopulationInLesserPoland().stream().forEach(s-> System.out.println(s.getName()));

//        System.out.println(countryRepository.findTheBiggest().getName());

       System.out.println(countryRepository.findAvgLifeExpectancyInEurope());

        while (state != State.EXIT) {
            switch (state) {
                case INIT: {
                    System.out.println("Opcje filtracji");
                    System.out.println("1.Panstwa");
                    System.out.println("2.Miata");
                    System.out.println("3.Wyjście");
                    String answer = scanner.next();
                    switch (answer) {
                        case "1": {
                            state = State.COUNTRY;
                            break;
                        }
                        case "2": {
                            state = State.CITY;
                            break;
                        }
                        case "3": {
                            state = State.EXIT;
                            break;
                        }
                        default: {
                            state = State.INIT;
                            break;
                        }
                    }
                    break;
                }
                case COUNTRY: {
                    System.out.println("Filtruj po");
                    System.out.println("1.Ludności");
                    System.out.println("2.Języku");
                    System.out.println("3.Miastach");
                    System.out.println("4.Wyjście");
                    String answer = scanner.next();
                    switch (answer) {
                        case "1": {

                            System.out.println("Minimalna ilość ludności");
                            Integer min = scanner.nextInt();
                            System.out.println("Maksymalna  ilość ludności");
                            Integer max = scanner.nextInt();

                            countryRepository.findCountriesByPopulation(min,max).stream().forEach(s->{
                                System.out.println(s.getName() + " " + s.getPopulation());
                            });

                            break;
                        }
                        case "2": {

                            System.out.println("Podaj język");
                            String langauge = scanner.next();

                            countryRepository.findCountryByLanguage(langauge).stream().forEach(s->{
                                System.out.println(s.getName());
                            });

                            break;

                        }

                        case "3": {

                            System.out.println("Podaj miasto");
                            String city = scanner.next();

                            countryRepository.findCountryByCity(city).stream().forEach(s->{
                                System.out.println(s.getName());
                            });

                            break;
                        }
                        case "4": {
                            state = State.EXIT;
                            break;
                        }
                        default: {
                            state = State.INIT;
                            break;
                        }
                        }

                    }
                case CITY:{
                    System.out.println("Filtruj po");
                    System.out.println("1.Województwie");
                    System.out.println("2.Kraju");
                    System.out.println("3.Języku");
                    String answer = scanner.next();
                    switch (answer){
                    case "1": {

                        System.out.println("Podaj województwo");
                        String district = scanner.next();

                        cityRepository.findCitiesByDistrict(district).stream().forEach(s->{
                            System.out.println(s.getName());
                        });

                        break;
                    }
                    case "2": {

                        System.out.println("Podaj Kraj");
                        String country = scanner.next();

                        cityRepository.findCitiesByCountry(country).stream().forEach(s->{
                            System.out.println(s.getName());
                        });

                        break;

                    }

                    case "3": {

                        System.out.println("Podaj język");
                        String language = scanner.next();

                        cityRepository.findCitiesByLanguage(language).stream().forEach(s->{
                            System.out.println(s.getName());
                        });

                        break;
                    }
                    case "4": {
                        state = State.EXIT;
                        break;
                        }
                    default: {
                        state = State.INIT;
                        break;
                    }
                }
                }

            }


                }

//        cityRepository.findCities(100000,200000).stream().forEach(s->{
//            System.out.println(s.getName() + " " + s.getPopulation());
//        });

//        countryRepository.findCountriesByPopulation(20000000,40000000).stream().forEach(s->{
//            System.out.println(s.getName() + " " + s.getPopulation());
//        });
//
//        countryRepository.findCountryByLanguage("English").stream().forEach(s->{
//            System.out.println(s.getName());
//        });

//        countryRepository.findCountryWithBoundries(1,20).stream().forEach(s->{
//            System.out.println(s.getName());
//        });
//
//        cityRepository.findCitiesByLanguage("Polish").stream().forEach(s->{
//                System.out.println(s.getName()); });

        return;
    }
}
