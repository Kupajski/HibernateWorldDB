package repository;

import models.City;
import models.TrueFalse;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

import static models.TrueFalse.T;

public class cityRepository extends GenericDao<City,Integer> {


    public cityRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public City create(City entity) {
        return super.create(entity);
    }

    @Override
    public City read(Integer id) {
        return super.read(id);
    }

    @Override
    public City update(City entity) {
        return super.update(entity);
    }

    @Override
    public void delete(City entity) {
        super.delete(entity);
    }


    public List<City> findCitiesByDistrict(String district){
        List<City> cities = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from City c where c.district = :district");
            query.setParameter("district", district);
            cities = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return cities;
    }

    public List<City> findCitiesByCountry(String country){
        List<City> cities = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from City c inner join c.countryCode co where co.name = :country ");
            query.setParameter("country", country);
            cities = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return cities;
    }

    public List<City> findCitiesByLanguage(String language){
        List<City> cities = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from City c inner join c.countryCode co inner join co.countryLanguages lg where lg.language = :language and lg.isOfficial = 'T' ");
            query.setParameter("language", language);
            cities = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return cities;
    }

    public List<City> findAllInPoland(){
        List<City> cities = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from City c inner join c.countryCode co where co.name ='Poland'");
            cities = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return cities;
    }

    public List<City> findAvgPopulationInLesserPoland(){
        List<City> cities = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from City c where c.district = 'Malopolskie' and c.countryCode = 'POL'");
            cities = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return cities;
    }





}
