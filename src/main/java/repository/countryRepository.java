package repository;

import models.City;
import models.Country;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class countryRepository extends GenericDao<Country,String> {



    public countryRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Country create(Country entity) {
        return super.create(entity);
    }

    @Override
    public Country read(String id) {
        return super.read(id);
    }

    @Override
    public Country update(Country entity) {
        return super.update(entity);
    }

    @Override
    public void delete(Country entity) {
        super.delete(entity);
    }

    public List<Country> findCountriesByPopulation(Integer min, Integer max){
        List<Country> countries = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c where c.population between :min and :max");
            query.setParameter("min", min);
            query.setParameter("max", max);
            countries = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return countries;
    }

    public List<Country> findCountryByLanguage(String language){
        List<Country> countries = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c inner join c.countryLanguages lg where lg.language = :language");
            query.setParameter("language", language);
            countries = query.getResultList();
            transaction.commit();
        } catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return countries;
    }

    public List<Country> findCountryByCity(String city){
        List<Country> countries = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c inner join c.cities ci where ci.name = :city");
            query.setParameter("city", city);
            countries = query.getResultList();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return countries;
    }

    public List<Country> findCountryWithBoundries(Integer first, Integer max){
        List<Country> countries = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c ");
            query.setFirstResult(first);
            query.setMaxResults(max);
            countries = query.getResultList();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return countries;
    }

    public List<Country> findAll(){
        List<Country> countries = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c ");
            countries = query.getResultList();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return countries;
    }

    public Country findTheBiggest(){
        Country country = null;
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c where c.surfaceArea = (select max(c1.surfaceArea) from Country c1 ) ");
            country = (Country) query.getSingleResult();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return country;
    }

    public Object findAvgLifeExpectancyInEurope(){
        Object country = null;
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select avg(c.lifeExpectancy) from Country c ");
            country =  query.getSingleResult();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return country;
    }

    public List<Country> findAllWithOfficialEnglish(){
        List<Country> countries = new ArrayList<>();
        EntityTransaction transaction = null;
        try{
            transaction = em.getTransaction();
            transaction.begin();
            Query query = em.createQuery("select c from Country c inner join c.countryLanguages lg where lg.language ='English' and lg.isOfficial='T'");
            countries = query.getResultList();
            transaction.commit();
        }catch (Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        return countries;
    }


}
