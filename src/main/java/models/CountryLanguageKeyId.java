package models;

import java.io.Serializable;
import java.util.Objects;


public class CountryLanguageKeyId implements Serializable {


    private String language;

    private Country countryCode;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Country getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Country countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryLanguageKeyId that = (CountryLanguageKeyId) o;
        return Objects.equals(language, that.language) &&
                Objects.equals(countryCode, that.countryCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(language, countryCode);
    }
}
