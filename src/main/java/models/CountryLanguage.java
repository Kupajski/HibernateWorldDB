package models;

import javax.persistence.*;

@Entity
@IdClass(CountryLanguageKeyId.class)
public class CountryLanguage {


    @Id
    private String language;

    @Id
    @ManyToOne
    @JoinColumn(name = "CountryCode")
    private Country countryCode;

    @Column(name = "isOfficial", nullable = false)
    private TrueFalse isOfficial;

    @Column(nullable = false)
    private Float percentage;
}
