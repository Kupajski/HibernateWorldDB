package models;

public enum TrueFalse {
    T,
    F
}
