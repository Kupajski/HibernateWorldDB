import models.City;
import models.Country;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.core.Every;
import org.junit.Test;
import repository.cityRepository;
import repository.countryRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import static org.junit.matchers.JUnitMatchers.*;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class cityRepositoryTest {


    @Test
    public void shouldReturnTrueWheneListContains44Elements(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        cityRepository cityRepository = new cityRepository(entityManager);

        List<City> list = cityRepository.findAllInPoland();

        assertThat(list, hasSize(44));
    }

    @Test
    public void shouldReturnTrueWheneAllCitiesAreFromPoland(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        cityRepository cityRepository = new cityRepository(entityManager);

        List<City> list = cityRepository.findAllInPoland();

        assertThat(list.toString(), containsString("Poland"));
    }
    @Test
    public void shouldReturnTrueWheneAllCitiesAreFromPoland2(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        cityRepository cityRepository = new cityRepository(entityManager);

        List<City> list = cityRepository.findAllInPoland();

        assertThat(list,contains(allOf(Matchers.hasProperty("countryCode" , equalTo("Poland")))));
    }

    @Test
    public void shouldReturnTrueWhenSizeIsTwo(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        cityRepository cityRepository = new cityRepository(entityManager);

        List<City> list = cityRepository.findAvgPopulationInLesserPoland();

        assertThat(list, hasSize(2));
    }

    @Test
    public void shouldReturnTrueWhenSizeIs239(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        countryRepository countryRepository = new countryRepository(entityManager);
        List<Country> list = countryRepository.findAll();

        assertThat(list, hasSize(239));
    }
    @Test
    public void shouldReturnTrueWhenEqualsRussianFederation(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        countryRepository countryRepository = new countryRepository(entityManager);
        Country list = countryRepository.findTheBiggest();

        assertThat(list.getName(), is("Russian Federation"));
    }
    @Test
    public void shouldReturnFalseWhenIsComparedToUSA(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        countryRepository countryRepository = new countryRepository(entityManager);
        Country list = countryRepository.findTheBiggest();

        assertThat(list.getName(), not("USA"));
    }
    @Test
    public void shouldReturnTrueWhenEqualsAvg(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        countryRepository countryRepository = new countryRepository(entityManager);
        Object avg = countryRepository.findAvgLifeExpectancyInEurope();

        assertThat(avg, is(66.48604));
    }
    @Test
    public void shouldReturnFalseWhen2TypedIn(){

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("examplePersistenceUnit");
        EntityManager entityManager = managerFactory.createEntityManager();

        countryRepository countryRepository = new countryRepository(entityManager);
        Object avg = countryRepository.findAvgLifeExpectancyInEurope();

        assertThat(avg, not(2));
    }

}
